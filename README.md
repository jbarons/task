## Task for MG-software

Implement P2P investment experience in code:
* User has previously funded a specific amount of money to his account. Account funding implementation is not needed.
* User can invest into different project opportunities.
* User can sell them back to platform at same price.
* User can see his own investment portfolio, and it's total value.
___
## Installation

* I recommend php7.4+
* clone the repository
* go into the directory
* composer install with dev (required if you want to run tests)
* create database (mysql or whatever laravel supported)
* copy .env.example to .env
* fill database credentials in .env
* to see how to use the code, you can read test classes
* run 'php artisan key:generate' to add key
* run 'php artisan serve' to run project in browser (not required)
* you can run tests (in nice format)  by running ./vendor/bin/pest

## Some notes about implementation

* I simplified money representation to just minimal measurement of currency (cents),
because money itself is big thing to deal with (Currencies, Rates, Conversations).
  For that we could possibly use package like this https://github.com/cknow/laravel-money

* I modified some interfaces, so it better fit with my understanding of functional requirements

* I simplified money flow logic. (calculating just current value) 
  (advanced would be to use transactions table), 
  
  so that always the value could be calculated from past transactions

* I assume that when you sell back investment (when sold, then object deleted)

* I used tests to test my code, for better experience I use pest instead of plain phpunit

* I assumed its part of bigger project, so I used approach that keeps that in mind (long term project)

* Its not perfect code, I know :)

___

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
