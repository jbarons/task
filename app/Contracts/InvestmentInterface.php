<?php

namespace App\Contracts;

interface InvestmentInterface
{
    /**
     * @return integer
     */
    public function getValue(): int;


    /**
     * @return integer
     */
    public function getShares(): int;


    /**
     * @return OpportunityInterface
     */
    public function getOpportunity(): OpportunityInterface;


    /**
     * @return mixed
     */
    public function getKey();
}
