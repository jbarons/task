<?php

namespace App\Contracts;

interface MoneyInterface
{
    /**
     * Represents smallest money unit (e.g. cents)
     *
     * @return int
     */
    public function getValue(): int;
}
