<?php

namespace App\Contracts;

interface OpportunityInterface
{

    /**
     * @return mixed
     */
    public function getKey();

    /**
     * @return integer
     */
    public function getShareValue(): int;

    /**
     * @return integer
     */
    public function getTotalShares(): int;

    /**
     * @return integer
     */
    public function getAvailableShares(): int;
}
