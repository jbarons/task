<?php

namespace App\Contracts;

interface PortfolioEntityInterface
{
    /**
     * @return integer
     */
    public function getTotalValue(): int;

    /**
     * @return mixed
     */
    public function getKey();

    /**
     * @return array
     */
    public function toArray();
}
