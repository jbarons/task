<?php

namespace App\Contracts;

use Illuminate\Support\Collection;

interface PortfolioInterface
{
    /**
     * @return InvestmentInterface[]
     */
    public function getInvestments(): Collection;

    /**
     * @param UserInterface $user
     * @param OpportunityInterface $opportunity
     * @param integer $shares
     * @return void
     */
    public function invest(UserInterface $user, OpportunityInterface $opportunity, int $shares): void;

    /**
     * User is able to sell investment
     *
     * @param UserInterface $user
     * @param InvestmentInterface $investment
     * @return void
     */
    public function sell(UserInterface $user, InvestmentInterface $investment): void;

    /**e
     * @return int
     */
    public function getTotalValue(): int;
}
