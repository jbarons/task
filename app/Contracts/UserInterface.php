<?php

namespace App\Contracts;

interface UserInterface
{
    /**
     * Returns user available funds (money)
     *
     * @return integer
     */
    public function getBalance(): int;

    /**
     * @return UserInterface
     */
    public function refreshFromSource(): UserInterface;

    /**
     * @return mixed
     */
    public function getKey();
}
