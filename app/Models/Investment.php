<?php

namespace App\Models;

use App\Contracts\InvestmentInterface;
use App\Contracts\OpportunityInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Investment extends Model implements InvestmentInterface
{
    use HasFactory;

    protected $fillable = [
        'opportunity_id',
        'portfolio_id',
        'shares',
        'value',
    ];

    protected $casts = [
        'shares' => 'int',
        'value' => 'int',
    ];

    public function opportunity(): BelongsTo
    {
        return $this->belongsTo(Opportunity::class);
    }

    public function transaction(): HasOne
    {
        return $this->hasOne(Transaction::class);
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getShares(): int
    {
        return $this->shares;
    }

    public function getOpportunity(): OpportunityInterface
    {
        $opportunity =  $this->opportunity()->first();

        if (! $opportunity) {
            return new Opportunity();
        }

        return $opportunity;
    }
}
