<?php

namespace App\Models;

use App\Contracts\OpportunityInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model implements OpportunityInterface
{
    use HasFactory;

    protected $fillable = [
        'share_value',
        'total_shares',
        'available_shares',
    ];

    protected $casts = [
        'share_value' => 'int',
        'total_shares' => 'int',
        'available_shares' => 'int',
    ];

    public function getAvailableShares(): int
    {
        return $this->available_shares;
    }

    public function getShareValue(): int
    {
        return $this->share_value;
    }

    public function getTotalShares(): int
    {
        return $this->total_shares;
    }
}
