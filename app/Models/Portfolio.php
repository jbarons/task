<?php

namespace App\Models;

use App\Contracts\PortfolioEntityInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Portfolio extends Model implements PortfolioEntityInterface
{
    use HasFactory;

    protected $fillable = [
        'total_value',
    ];

    protected $casts = [
        'total_value' => 'int'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function investments(): HasMany
    {
        return $this->hasMany(Investment::class);
    }

    public function getTotalValue(): int
    {
        return $this->total_value;
    }
}
