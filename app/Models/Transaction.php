<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'investment_id',
        'key',
        'user_id'
    ];

    protected $casts = [
        'amount' => 'int'
    ];

    protected $with = [
        'investment',
        'user'
    ];

    public function getIdentifier(): string
    {
        return $this->getKey();
    }

    public function investment(): BelongsTo
    {
        return $this->belongsTo(Investment::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
