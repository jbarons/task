<?php

namespace App\P2P\Repositories\Contracts;

use App\Contracts\InvestmentInterface;

interface InvestmentRepositoryInterface
{
    /**
     * @param array $attributes
     * @return InvestmentInterface
     */
    public function create(array $attributes): InvestmentInterface;

    /**
     * @param $key
     * @param array $attributes
     * @return bool
     */
    public function update($key, array $attributes): bool;

    /**
     * @param $key
     * @return bool
     */
    public function delete($key): bool;
}
