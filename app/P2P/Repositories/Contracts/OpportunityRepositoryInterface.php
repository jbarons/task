<?php

namespace App\P2P\Repositories\Contracts;

interface OpportunityRepositoryInterface
{
    /**
     * @param $key
     * @param array $attributes
     * @return bool
     */
    public function update($key, array $attributes): bool;
}
