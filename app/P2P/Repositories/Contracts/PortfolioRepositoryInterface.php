<?php

namespace App\P2P\Repositories\Contracts;

use App\Contracts\PortfolioEntityInterface;
use Illuminate\Support\Collection;

interface PortfolioRepositoryInterface
{
    /**
     * @param $key
     * @return PortfolioEntityInterface
     */
    public function getByPrimaryKey($key): PortfolioEntityInterface;

    /**
     * @param $key
     * @return Collection
     */
    public function getInvestments($key): Collection;
}
