<?php

namespace App\P2P\Repositories\Contracts;

use App\Contracts\UserInterface;

interface UserRepositoryInterface
{
    /**
     * @param int $key
     * @return UserInterface
     */
    public function findByPrimaryKey(int $key): UserInterface;

    /**
     * @param $key
     * @param int $value
     * @return UserInterface
     */
    public function increaseBalance($key, int $value): UserInterface;

    /**
     * @param $key
     * @param int $value
     * @return UserInterface
     */
    public function decreaseBalance($key, int $value): UserInterface;
}
