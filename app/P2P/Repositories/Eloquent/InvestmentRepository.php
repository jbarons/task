<?php

namespace App\P2P\Repositories\Eloquent;

use App\Contracts\InvestmentInterface;
use App\Models\Investment;
use App\P2P\Repositories\Contracts\InvestmentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class InvestmentRepository implements InvestmentRepositoryInterface
{
    /**
     * @var Model
     */
    private $investment;

    /**
     * InvestmentRepository constructor.
     * @param Investment $investment
     */
    public function __construct(Investment $investment)
    {
        $this->investment = $investment;
    }

    /**
     * @param array $attributes
     * @return InvestmentInterface
     */
    public function create(array $attributes): InvestmentInterface
    {
        return $this->investment->create($attributes);
    }

    /**
     * @param $key
     * @param array $attributes
     * @return bool
     */
    public function update($key, array $attributes): bool
    {
        $investment = $this->investment->newQuery()->find($key);

        if (! $investment) {
            return false;
        }

        return (bool) $investment->update($attributes);
    }

    /**
     * @param $key
     * @return bool
     */
    public function delete($key): bool
    {
        $model = $this->investment->newQuery()->find($key);

        if (! $model) {
            return false;
        }
        return (bool) $model->delete();
    }
}
