<?php

namespace App\P2P\Repositories\Eloquent;

use App\Models\Opportunity;
use App\P2P\Repositories\Contracts\OpportunityRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class OpportunityRepository implements OpportunityRepositoryInterface
{
    /**
     * @var Model
     */
    private $opportunity;

    /**
     * OpportunityRepository constructor.
     * @param Opportunity $opportunity
     */
    public function __construct(Opportunity $opportunity)
    {
        $this->opportunity = $opportunity;
    }

    public function update($key, array $attributes): bool
    {
        $opportunity = $this->opportunity->newQuery()->find($key);

        if (! $opportunity) {
            return false;
        }

        return $opportunity->update($attributes);
    }
}
