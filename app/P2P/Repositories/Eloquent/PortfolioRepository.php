<?php

namespace App\P2P\Repositories\Eloquent;

use App\Contracts\InvestmentInterface;
use App\Contracts\PortfolioEntityInterface;
use App\Models\Portfolio;
use App\P2P\Repositories\Contracts\PortfolioRepositoryInterface;
use Illuminate\Support\Collection;

class PortfolioRepository implements PortfolioRepositoryInterface
{
    /**
     * @var Portfolio
     */
    private $model;

    /**
     * PortfolioRepository constructor.
     * @param Portfolio $model
     */
    public function __construct(Portfolio $model)
    {
        $this->model = $model;
    }

    public function getByPrimaryKey($key): PortfolioEntityInterface
    {
        return $this->model->find($key)->first();
    }

    /**
     * @return InvestmentInterface[]
     */
    public function getInvestments($key): Collection
    {
        $portfolio = $this->model->newQuery()->find($key);

        if (! $portfolio) {
            return collect([]);
        }

        return $portfolio->investments()->get();
    }
}
