<?php

namespace App\P2P\Repositories\Eloquent;

use App\Contracts\UserInterface;
use App\Models\User;
use App\P2P\Repositories\Contracts\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function findByPrimaryKey(int $key): UserInterface
    {
        return $this->model->find($key);
    }

    public function update($key, array $attributes): bool
    {
        return $this->model->find($key)->update($attributes);
    }

    public function increaseBalance($key, int $value): UserInterface
    {
        $account = $this->findByPrimaryKey($key);

        $this->update($key, [
           'balance' => $account->getBalance() + $value
        ]);

        return $this->findByPrimaryKey($key);
    }

    public function decreaseBalance($key, int $value): UserInterface
    {
        $account = $this->findByPrimaryKey($key);

        $this->update($key, [
            'balance' => $account->getBalance() - $value
        ]);

        return $this->findByPrimaryKey($key);
    }
}
