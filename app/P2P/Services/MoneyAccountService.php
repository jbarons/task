<?php

namespace App\P2P\Services;

use App\Contracts\UserInterface;
use App\P2P\Exceptions\NotEnoughFundsException;
use App\P2P\Exceptions\UserNotInitializedException;
use App\P2P\Repositories\Contracts\UserRepositoryInterface;

class MoneyAccountService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var UserInterface|null
     */
    private $account;
    /**
     * MoneyAccountService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserInterface $user
     * @return MoneyAccountService
     */
    public function accountFor(UserInterface $user)
    {
        $this->account = $user;

        return $this;
    }

    /**
     * @return int
     * @throws UserNotInitializedException
     */
    public function getBalance(): int
    {
        $this->checkUser();

        return $this->account->getBalance();
    }

    /**
     * @param int $amount
     * @return MoneyAccountService
     * @throws UserNotInitializedException
     * @throws NotEnoughFundsException
     */
    public function charge(int $amount): MoneyAccountService
    {
        $this->checkUser();

        if ($amount > $this->fresh()->getBalance()) {
            throw new NotEnoughFundsException('Not enough money');
        }

        $this->userRepository->decreaseBalance($this->account->getKey(), $amount);

        return $this;
    }

    /**
     * @param int $amount
     * @return MoneyAccountService
     * @throws UserNotInitializedException
     */
    public function credit(int $amount): MoneyAccountService
    {
        $this->checkUser();

        $this->userRepository->increaseBalance($this->account->getKey(), $amount);

        return $this;
    }

    /**
     * @return $this
     */
    public function fresh(): MoneyAccountService
    {
         $this->account = $this->account->refreshFromSource();

         return $this;
    }

    /**
     * @throws UserNotInitializedException
     */
    private function checkUser(): void
    {
        if (!$this->account) {
            throw new UserNotInitializedException('User not initialized for this class instance');
        }
    }
}
