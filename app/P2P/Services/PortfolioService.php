<?php

namespace App\P2P\Services;

use App\Contracts\InvestmentInterface;
use App\Contracts\OpportunityInterface;
use App\Contracts\PortfolioEntityInterface;
use App\Contracts\PortfolioInterface;
use App\Contracts\UserInterface;
use App\P2P\Exceptions\NotEnoughSharesException;
use App\P2P\Repositories\Contracts\InvestmentRepositoryInterface;
use App\P2P\Repositories\Contracts\OpportunityRepositoryInterface;
use App\P2P\Repositories\Contracts\PortfolioRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PortfolioService implements PortfolioInterface
{
    /**
     * @var PortfolioRepositoryInterface
     */
    private $portfolioRep;

    /**
     * @var PortfolioEntityInterface|null
     */
    private $portfolio;

    /**
     * @var InvestmentRepositoryInterface
     */
    private $investmentRep;

    /**
     * @var OpportunityRepositoryInterface
     */
    private $opportunityRep;

    /**
     * @var MoneyAccountService
     */
    private $accountService;

    /**
     * PortfolioService constructor.
     * @param InvestmentRepositoryInterface $investmentRep
     * @param PortfolioRepositoryInterface $portfolio
     */
    public function __construct(
        InvestmentRepositoryInterface $investmentRep,
        PortfolioRepositoryInterface $portfolio,
        OpportunityRepositoryInterface $opportunityRep,
        MoneyAccountService $accountService
    ) {
        $this->portfolioRep = $portfolio;
        $this->investmentRep = $investmentRep;
        $this->opportunityRep = $opportunityRep;
        $this->accountService = $accountService;
    }

    /**
     * @param mixed $primaryKey
     * @return PortfolioInterface
     */
    public function initFor($primaryKey): PortfolioService
    {
        $this->portfolio = $this->portfolioRep->getByPrimaryKey($primaryKey);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getInvestments(): Collection
    {
        return $this->portfolioRep->getInvestments($this->portfolio->getKey());
    }

    /**
     * @inheritDoc
     */
    public function invest(UserInterface $user, OpportunityInterface $opportunity, int $shares): void
    {
        $investment = $opportunity->getShareValue() * $shares;

        DB::beginTransaction();
        try {
            $this->accountService->accountFor($user)->charge($investment);
            $this->createInvestment($opportunity, $shares);
            $this->decreaseShares($opportunity, $shares);
        } catch (\Exception $exception) {
            DB::rollBack();
        }

        DB::commit();
    }

    /**
     * @inheritDoc
     */
    public function sell(UserInterface $account, InvestmentInterface $investment): void
    {
        DB::beginTransaction();
        try {
            $this->increaseShares($investment->getOpportunity(), $investment->getShares());

            $this->investmentRep->delete($investment->getKey());

            $this->accountService->accountFor($account)->credit(
                $investment->getValue() * $investment->getShares()
            );

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * @param OpportunityInterface $opportunity
     * @param int $shares
     */
    private function createInvestment(OpportunityInterface $opportunity, int $shares): void
    {
        $this->investmentRep->create([
            'opportunity_id' => $opportunity->getKey(),
            'portfolio_id' => $this->portfolio->getKey(),
            'shares' => $shares,
            'value' => $opportunity->getShareValue(),
        ]);
    }

    /**
     * @param OpportunityInterface $opportunity
     * @param int $shares
     * @return void
     */
    private function increaseShares(OpportunityInterface $opportunity, int $shares): void
    {
        $shares = $opportunity->getAvailableShares() + $shares;

        $this->updateAvailableShares($opportunity->getKey(), $shares);
    }

    /**
     * @param OpportunityInterface $opportunity
     * @param int $shares
     * @return void
     * @throws NotEnoughSharesException
     */
    private function decreaseShares(OpportunityInterface $opportunity, int $shares): void
    {
        $shares = $opportunity->getAvailableShares() - $shares;

        if ($shares > $opportunity->getAvailableShares()) {
            throw new NotEnoughSharesException('Not enough shares');
        }

        $this->updateAvailableShares($opportunity->getKey(), $shares);
    }

    /**
     * @param $key
     * @param int $newValue
     * @return void
     */
    private function updateAvailableShares($key, int $newValue): void
    {
        $this->opportunityRep->update($key, [
            'available_shares' => $newValue
        ]);
    }

    /**
     * @return int
     */
    public function getTotalValue(): int
    {
        return $this->getInvestments()
            ->sum(fn($investment) => $investment->getValue() * $investment->getShares());
    }
}
