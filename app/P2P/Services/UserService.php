<?php

namespace App\P2P\Services;

use App\Contracts\PortfolioInterface;
use App\Contracts\UserInterface;
use App\P2P\Repositories\Contracts\PortfolioRepositoryInterface;
use App\P2P\Repositories\Contracts\UserRepositoryInterface;

class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var PortfolioRepositoryInterface
     */
    private $portfolioRepository;

    /**
     * @var UserInterface
     */
    private $userAccount;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepo
     * @param PortfolioRepositoryInterface $portfolioRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepo,
        PortfolioRepositoryInterface $portfolioRepository
    ) {
        $this->userRepo = $userRepo;
        $this->portfolioRepository = $portfolioRepository;
    }

    public function profile($key): UserService
    {
        $this->userAccount = $this->userRepo->findByPrimaryKey($key);

        return $this;
    }

    public function getBalance(): int
    {
        return $this->userRepo->getBalance();
    }

    /**
     * @throws \Exception
     */
    public function getPortfolio(): PortfolioInterface
    {
        if (! $this->userAccount) {
            throw new \Exception('User service is not initialized for specific account');
        }

        return resolve(PortfolioInterface::class);
    }

    public function charge(int $value): void
    {
        if ($this->getBalance() < $value) {
            throw new \Exception("Not enough funds");
        }

        $this->userRepo->update([
           'balance' => $this->getBalance() - $value
        ]);
    }


    public function credit(int $value): void
    {
        $this->user->update([
            'balance' => $this->getBalance() + $value
        ]);
    }
}
