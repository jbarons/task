<?php

namespace App\Providers;

use App\P2P\Investment;
use App\P2P\Repositories\Contracts\InvestmentRepositoryInterface;
use App\P2P\Repositories\Contracts\OpportunityRepositoryInterface;
use App\P2P\Repositories\Contracts\PortfolioRepositoryInterface;
use App\P2P\Repositories\Contracts\UserRepositoryInterface;
use App\P2P\Repositories\Eloquent\InvestmentRepository;
use App\P2P\Repositories\Eloquent\OpportunityRepository;
use App\P2P\Repositories\Eloquent\PortfolioRepository;
use App\P2P\Repositories\Eloquent\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(InvestmentRepositoryInterface::class, InvestmentRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(OpportunityRepositoryInterface::class, OpportunityRepository::class);
        $this->app->bind(PortfolioRepositoryInterface::class, PortfolioRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
