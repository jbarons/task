<?php

namespace Database\Factories;

use App\Models\Investment;
use App\Models\Opportunity;
use App\Models\Portfolio;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvestmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Investment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => $this->faker->numberBetween(1, 1000),
            'shares' => $this->faker->numberBetween(1, 1000),
            'portfolio_id' => Portfolio::factory()->create(),
            'opportunity_id' => Opportunity::factory()->create(),
        ];
    }
}
