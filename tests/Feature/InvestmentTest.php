<?php

namespace Tests\Feature;

use App\Models\Investment;
use App\Models\Opportunity;
use App\Models\Portfolio;
use App\P2P\Repositories\Contracts\InvestmentRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InvestmentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var InvestmentRepositoryInterface
     */
    protected $investmentRepo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->investmentRepo = resolve(InvestmentRepositoryInterface::class);
    }

    public function testItCanCreateInvestment(): void
    {
        $data = [
            'opportunity_id' => Opportunity::factory()->create(),
            'portfolio_id' => Portfolio::factory()->create(),
            'shares' => 100,
            'value' => 20
        ];

        $this->investmentRepo->create($data);

        $this->assertDatabaseHas('investments', $data);
    }

    public function testItCanUpdateInvestment(): void
    {
        $investment = Investment::factory()->create();

        $this->investmentRepo->update($investment->getKey(), [
            'value' => 700,
        ]);

        $this->assertDatabaseHas('investments', [
            'id' => 1,
            'value' => 700
        ]);
    }

    public function testItCanDeleteTheInvestment(): void
    {
        $investment = Investment::factory()->create([
            'value' => 700
        ]);

        $this->investmentRepo->delete($investment->getKey());

        $this->assertDataBaseMissing('investments', [
            'id' => 1,
            'value' => 700
        ]);
    }
}
