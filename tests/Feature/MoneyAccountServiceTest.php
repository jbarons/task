<?php

namespace Tests\Feature;

use App\Models\User;
use App\P2P\Exceptions\NotEnoughFundsException;
use App\P2P\Services\MoneyAccountService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MoneyAccountServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var MoneyAccountService
     */
    protected $moneyAccountService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->moneyAccountService = resolve(MoneyAccountService::class);
    }

    public function testItBeChargedWhenAlreadyHasFunds(): void
    {
        $user = User::factory()->create([
            'balance' => 1000,
        ]);

        $newBalance = $this->moneyAccountService->accountFor($user)
           ->charge(100)
           ->fresh()
           ->getBalance();

        $this->assertEquals(900, $newBalance);
    }

    public function testItFailsToChargeWhenNotEnoughFunds(): void
    {
        $this->expectException(NotEnoughFundsException::class);

        $user = User::factory()->create([
            'balance' => 10,
        ]);

        $this->moneyAccountService->accountFor($user)
            ->charge(100)
            ->fresh()
            ->getBalance();
    }

    public function testItCanBeCredited(): void
    {
         $newBalance = $this->moneyAccountService->accountFor(
             User::factory()->create(['balance' => 10])
         )->credit(1000)->fresh()
         ->getBalance();

         $this->assertEquals(1010, $newBalance);

         $this->assertDatabaseHas('users', [
             'balance' => $newBalance
         ]);
    }
}
