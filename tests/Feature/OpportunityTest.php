<?php

namespace Tests\Feature;

use App\Models\Opportunity;
use App\P2P\Repositories\Contracts\OpportunityRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OpportunityTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var OpportunityRepositoryInterface
     */
    protected $opportunityRepo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->opportunityRepo = resolve(OpportunityRepositoryInterface::class);
    }

    public function testItCanUpdateOpportunity(): void
    {
        $opportunity = Opportunity::factory()->create();

         $this->opportunityRepo->update($opportunity->getKey(), [
            'share_value' => 700,
         ]);

        $this->assertDatabaseHas('opportunities', [
            'id' => 1,
            'share_value' => 700
        ]);
    }
}
