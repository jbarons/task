<?php

namespace Tests\Feature;

use App\Models\Investment;
use App\Models\Opportunity;
use App\Models\Portfolio;
use App\Models\User;
use App\P2P\Services\PortfolioService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PortfolioServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var PortfolioService
     */
    protected $portfolioService;

    public function testItReturnsInvestments(): void
    {
        $user = User::factory()->create();
        $portfolio = Portfolio::factory()->create([
            'user_id' => $user
        ]);

        $opportunity = Opportunity::factory()->create();

        Investment::factory(3)->create([
            'portfolio_id' => $portfolio,
            'opportunity_id' => $opportunity,
        ]);

        $investments = $this->portfolioService
            ->initFor($portfolio->getKey())
            ->getInvestments();

        $this->assertCount(3, $investments);
    }

    public function testItCanInvest(): void
    {
        $user = User::factory()->create([
            'balance' => 1100
        ]);
        $portfolio = Portfolio::factory()->create([
            'user_id' => $user
        ]);

        $opportunityProject = Opportunity::factory()->create([
            'share_value' => 100,
            'total_shares' => 10,
            'available_shares' => 10,
        ]);

        $this->portfolioService
            ->initFor($portfolio)
            ->invest($user, $opportunityProject, 10);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'balance' => 100
        ]);

        $portfolioService = $this->portfolioService
            ->initFor($portfolio->getKey());
        $investments = $portfolioService
            ->getInvestments();

        $this->assertCount(1, $investments);
        $this->assertDatabaseHas('investments', [
            'value' => '100',
            'shares' => '10',
        ]);

        $this->assertEquals(1000, $portfolioService->getTotalValue());
    }

    public function testItCanSellTheInvestment(): void
    {
        $user = User::factory()->create();
        $portfolio = Portfolio::factory()->create([
            'user_id' => $user
        ]);

        $opportunity = Opportunity::factory()->create();

        $previousInvestments = Investment::factory(3)->create([
            'portfolio_id' => $portfolio,
            'opportunity_id' => $opportunity,
            'value' => 10,
            'shares' => 2
        ]);

        $this->portfolioService
            ->sell($user, $previousInvestments->first());

        $portfolioService = $this->portfolioService->initFor($user);
        $investments = $portfolioService->getInvestments();

        $this->assertCount(2, $investments);
        $this->assertEquals(40, $portfolioService->getTotalValue());
    }

    public function testItIncreasesSharesToOpportunityWhenInvestmentSold(): void
    {
        $user = User::factory()->create();
        $portfolio = Portfolio::factory()->create([
            'user_id' => $user
        ]);

        $opportunity = Opportunity::factory()->create([
            'total_shares' => 1000,
            'available_shares' => 100
        ]);

        $previousInvestments = Investment::factory(3)->create([
            'portfolio_id' => $portfolio,
            'opportunity_id' => $opportunity,
            'value' => 10,
            'shares' => 2
        ]);

        $portfolioService = $this->portfolioService->initFor($user);

        $portfolioService->sell($user, $previousInvestments->first());

        $this->assertEquals(102, $opportunity->fresh()->getAvailableShares());
    }

    public function testItCalculatesTotalValue(): void
    {
        $user = User::factory()->create();
        $portfolio = Portfolio::factory()->create([
            'user_id' => $user
        ]);

        $opportunity = Opportunity::factory()->create();

        Investment::factory(3)->create([
            'portfolio_id' => $portfolio,
            'opportunity_id' => $opportunity,
            'value' => 20,
            'shares' => 2
        ]);

        $portfolioService = $this->portfolioService->initFor($user);

        $this->assertEquals(120, $portfolioService->getTotalValue());
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->portfolioService = resolve(PortfolioService::class);
    }
}
