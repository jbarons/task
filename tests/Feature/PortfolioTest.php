<?php

namespace Tests\Feature;

use App\Models\Investment;
use App\Models\Portfolio;
use App\Models\User;
use App\P2P\Repositories\Contracts\PortfolioRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PortfolioTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var PortfolioRepositoryInterface
     */
    protected $portfolioRepo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->portfolioRepo = resolve(PortfolioRepositoryInterface::class);
    }

    public function testItCanGetByPrimaryKey(): void
    {
        $portfolio = Portfolio::factory()->create();

        $result = $this->portfolioRepo->getByPrimaryKey($portfolio->getKey());

        $this->assertEquals($portfolio->toArray(), $result->toArray());
    }

    public function testItCanGetInvestments(): void
    {
        $user = User::factory()->create();
        $portfolio = Portfolio::factory()->create([
            'user_id' => $user
        ]);

        Investment::factory(3)->create([
            'portfolio_id' => $portfolio,
        ]);

        $this->assertCount(3, $this->portfolioRepo->getInvestments($user->getKey()));
    }
}
