<?php

namespace Tests\Feature;

use App\Contracts\UserInterface;
use App\Models\User;
use App\P2P\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userRepository = resolve(UserRepositoryInterface::class);


 //    public function increaseBalance($key, int $value): UserInterface;
//    public function decreaseBalance($key, int $value): UserInterface;
    }

    public function testItCanGetByPrimaryKey(): void
    {
        $user = User::factory()->create();

        $result = $this->userRepository->findByPrimaryKey($user->getKey());

        $this->assertEquals($user->toArray(), $result->toArray());
    }

    public function testitCanIncreaseBalance(): void
    {
        $user = User::factory()->create([
            'balance' => 100
        ]);

        $result = $this->userRepository->increaseBalance($user->getKey(), 100);

        $this->assertEquals(200, $result->getBalance());
    }

    public function testItCanDecreaseBalance(): void
    {
        $user = User::factory()->create([
            'balance' => 150
        ]);

        $result = $this->userRepository->decreaseBalance($user->getKey(), 50);

        $this->assertEquals(100, $result->getBalance());
    }
}
